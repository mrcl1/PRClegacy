# ProjectCarthage Legacy
An old version of ProjectCarthage, the plugin that powers MRCL. 
The code in this repo is from the git tag V1. This is before the concept of VirtualWorld was introduced in PRC.

# Caveats
PRC V1 was highly dependent on other systems for it's operation, like commandblocks to activate/deactivate towers.

# Documentation
There is none, and the maintainers of this repo dont intend to provide any.
If you wish to document PRC V1, feel free to contact us. 

# Contributing
We don't expect this codebase to change, it's provided mostly for reference. If however you wish to expand on it, feel free to Merge Request improvements. 