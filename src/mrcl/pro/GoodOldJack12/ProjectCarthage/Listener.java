package mrcl.pro.GoodOldJack12.ProjectCarthage;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.LyokoUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import static mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Advancements.hasAdvancement;

public class Listener implements org.bukkit.event.Listener{
    private Plugin plugin = Bukkit.getPluginManager().getPlugin("ProjectCarthage");

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        if(player.isOp()){
            player.sendMessage("If you're OP and you get this message, Project Carthage Works!");
        }
        event.setJoinMessage(String.format(ChatColor.AQUA+"%s has been virtualized on MRCL",player.getDisplayName()));
        if(!hasAdvancement(player, "mrcl:root")) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "advancement grant " + player.getName() + " only mrcl:root" );
        }
    }
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event){
        if (LyokoUtil.getLyokoplayers().contains(event.getEntity())){
            LyokoUtil.devirtualize(event.getEntity());
        }
    }
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event){
        final Player player = event.getPlayer();
        if (LyokoUtil.getDevirtedplayers().contains(event.getPlayer())){
            new BukkitRunnable(){
                @Override
                public void run() {
                    LyokoUtil.returnToScanner(player);
                    LyokoUtil.getDevirtedplayers().remove(player);
                }
            }.runTaskLater(plugin,20);
        }
        try {
            final Team team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("Lyoko");
            new BukkitRunnable(){
                @Override
                public void run() {
                    if(team.hasPlayer(player)) {
                        team.removePlayer(player);
                    }
                }
            }.runTaskLater(plugin,10);

        }catch (NullPointerException e){
        }
    }


    



}
