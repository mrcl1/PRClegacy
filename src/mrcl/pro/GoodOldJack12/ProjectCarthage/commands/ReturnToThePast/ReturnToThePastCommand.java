package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.ReturnToThePast;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Exceptions;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by jack on 4/23/18.
 */
public class ReturnToThePastCommand extends CarthageCommand {
    static HashMap<Player, Location> locations = new HashMap<>();

    public ReturnToThePastCommand(Main plugin) {
        super(plugin, "rttp");
        this.setPermission("ProjectCarthage.rttp");
        this.setHelpMessage(ChatColor.YELLOW + "usage: /rttp [user]\n" +
                "Time travel and shit");
        this.addSubCommand(new rttpIgnore(plugin));
        this.addSubCommand(new rttpforce(plugin));
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender, command, s, strings);

        try {
            Checker.checkPerm(commandSender, getPermission());
            Checker.checkarglength(strings, 0, 2);
            if (strings.length > 0) {
                try {
                    checkSubCommands(strings[0]);
                } catch (Checker.InvalidArgumentException e) {
                    Player player = Bukkit.getPlayer(strings[0]);
                    if (player != null) {
                        commandSender = player;
                        superscan.getReturnToThePast().activate(false);
                        Bukkit.dispatchCommand(plugin.getServer().getConsoleSender(), "advancement grant " + commandSender.getName() + " only mrcl:ach10");
                    } else {
                        commandSender.sendMessage(ChatColor.RED + "Invalid player!");
                        throw new Checker.InvalidArgumentException();
                    }

                }
            } else {
                superscan.getReturnToThePast().activate(false);
                if (commandSender instanceof Player) {
                    Bukkit.dispatchCommand(plugin.getServer().getConsoleSender(), "advancement grant " + commandSender.getName() + " only mrcl:ach10");
                }
            }
        } catch (Exceptions.CooldownException e) {
            commandSender.sendMessage(ChatColor.RED + "RTTP is unavailable! Try again in: " + ChatColor.BLUE + Math.floor(e.getRemaining() / 60 / 1000) + ChatColor.RED + " minutes.");
        } catch (Exceptions.TowerActivated e) {
            commandSender.sendMessage(ChatColor.RED + "RTTP unavailable! tower activated!");
        } catch (Exception e) {
            catchException(e);
        }
        return true;
    }

}
