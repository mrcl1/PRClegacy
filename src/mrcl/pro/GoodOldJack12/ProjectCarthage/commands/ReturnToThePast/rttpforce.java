package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.ReturnToThePast;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 5/4/18.
 */
public class rttpforce extends CarthageCommand {
    public rttpforce(Main plugin) {
        super(plugin, "force");
        this.setPermission("ProjectCarthage.rttp.force");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender, command, s, strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            lyoko.getSuperscan().getReturnToThePast().activate(true);
            commandSender.sendMessage(ChatColor.GREEN+"Return to the past now!");
        } catch (Exception e) {
            catchException(e);
        }
        return true;
    }
}
