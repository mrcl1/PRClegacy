package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.ReturnToThePast;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by jack on 5/3/18.
 */
public class rttpIgnore extends CarthageCommand {
    public rttpIgnore(Main plugin) {
        super(plugin, "ignoreme");
        this.setPermission("ProjectCarthage.rttp.ignore");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /rttp ignore <player> \n" +
                "Adds or removes you, or a given player, to the ignore list");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender, command, s, strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            Checker.checkarglength(strings,1);
            ignorePlayer(strings[0]);
        } catch (Checker.TooLittleArgsException e) {
            ignoreSender(commandSender);
        }catch (Exception e){
            catchException(e);
        }

        return true;
    }

    private void ignoreSender(CommandSender commandSender){
        try {
            Checker.checkPlayer(commandSender);
            if(superscan.getReturnToThePast().getIgnorePlayers().contains((Player)commandSender)){
                superscan.getReturnToThePast().getIgnorePlayers().remove((Player)commandSender);
                commandSender.sendMessage(ChatColor.GREEN+"You've been removed from the ignore list");

            }else {
                superscan.getReturnToThePast().getIgnorePlayers().add((Player) commandSender);
                commandSender.sendMessage(ChatColor.GREEN+"You've been added to the ignore list");
            }

        } catch (Exception e) {
            catchException(e);
        }
    }
    private void ignorePlayer(String playerstring){
        Player player;
        try {
            try {
                player = Bukkit.getPlayer(playerstring);
            }catch (NullPointerException e){throw new Checker.InvalidArgumentException();}
            if(superscan.getReturnToThePast().getIgnorePlayers().contains(player)){
                superscan.getReturnToThePast().getIgnorePlayers().remove(player);
                commandSender.sendMessage(String.format(ChatColor.GREEN+"Player %s has been removed from the ignore list",player.getName()));

            }else {
                superscan.getReturnToThePast().getIgnorePlayers().add(player);
                commandSender.sendMessage(String.format(ChatColor.GREEN+"Player %s has been added to the ignore list",player.getName()));
            }
        }catch (Exception e){
            catchException(e);
        }
    }
}
