package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.MiscCommands;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class fakedeactivate extends CarthageCommand {
    Checker checker = new Checker();
    public fakedeactivate(Main plugin) {
        super(plugin,"fakedeactivate");
        this.setHelpMessage(String.format(ChatColor.YELLOW+"usage: /fakedeactivate <sector> <tower>"));
        this.setPermission("ProjectCarthage.fakedeactivate");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
            registerSender(commandSender, command, s, strings);
            try {
                if(commandSender instanceof Player) {
                    Checker.checkPerm((Player) commandSender,getPermission());
                }
                Checker.checkarglength(strings,2,2);
                //End checks

                String tower = strings[0];
                String number = strings[1];
                deactivateTower(tower, number);
            } catch (Exception e) {
                catchException(e);
            }
    return true;
    }

    private void deactivateTower(String sector, String number){
        String superscanString = ChatColor.DARK_GREEN+"Superscan"+ChatColor.WHITE;
        plugin.getServer().broadcastMessage(String.format("%s: %s #%s was deactivated",superscanString,sector,number));
    }

}
