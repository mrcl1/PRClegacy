package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.MiscCommands;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class fakeactivate extends CarthageCommand {
    String[] activators = {"H","J","X"," "};
    public fakeactivate(Main plugin) {
        super(plugin,"fakeactivate");
        this.setPermission("ProjectCarthage.fakeactivate");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            try {
                if(commandSender instanceof Player){
                    Checker.checkPerm((Player)commandSender,getPermission());
                }
                Checker.checkarglength(strings,2,3);
                String sector = strings[0];
                String number = strings[1];
                char activator = 'X';
                if(strings.length > 2){
                    Checker.checkArgSanity(strings[2],activators);
                    activator = strings[2].charAt(0);
                }

                activateTower(sector,number,activator);
            } catch (Exception e) {
                catchException(e);
            }
        }catch (Exception e){
            commandSender.sendMessage("something went very wrong owo");
        }
        return true;
    }
    private void activateTower(String sector, String number, char activator){
        String superscanString = ChatColor.DARK_GREEN+"Superscan"+ChatColor.WHITE;
        String activatorString = ChatColor.RED+"XANA";
        switch (activator){
            case 'J': activatorString = ChatColor.AQUA+"Jeremie"; break;
            case 'H': activatorString = ChatColor.BLUE+"Hopper"; break;
        }
        plugin.getServer().broadcastMessage(String.format("%s: %s #%s was activated by %s!",superscanString,sector,number,activatorString));
    }
}
