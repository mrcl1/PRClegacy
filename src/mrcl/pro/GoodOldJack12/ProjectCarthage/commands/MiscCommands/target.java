package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.MiscCommands;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Formatter;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Targeter;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class target extends CarthageCommand {
    public target(Main plugin) {
        super(plugin, "target");
        this.setPermission("ProjectCarthage.target");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        super.registerSender(commandSender, command, s, strings);
        try {
            try {
                Checker.checkPlayer(commandSender);
                Checker.checkPerm((Player) commandSender, this.getPermission());
                Block targetblock = Targeter.getTargetblock((Player) commandSender);
                Material material = targetblock.getType();
                commandSender.sendMessage(String.format("You are looking at %s on the location: %s",material.toString(), Formatter.LocationFormat(targetblock.getLocation())));
                try {
                    commandSender.sendMessage(superscan.getInteractor(targetblock).toString());
                }catch (Superscan.InteractorDoesNotExistException e){
                    //do nothing
                }
            } catch (Exception e) {
                catchException(e);
            }
        } catch (Exception e) {
            commandSender.sendMessage(ChatColor.YELLOW+"Something went very wrong owo");
        }
        return true;
    }

}
