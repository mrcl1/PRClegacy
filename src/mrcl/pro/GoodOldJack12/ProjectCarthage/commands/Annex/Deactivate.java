package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 4/6/18.
 */
public class Deactivate extends CarthageCommand{
    public Deactivate(Main plugin) {
        super(plugin, "deactivate");
        this.setPermission("ProjectCarthage.annex.deactivate");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /anex deactivate <Sector> <Tower> \n" +
                "Deactivate a tower\nAliasses:\n de");
        this.addAlias("de");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            Checker.checkarglength(strings,2);
            Sector sector = superscan.getLyoko().findSector(strings[0]);
            Tower tower = sector.findTower(Integer.parseInt(strings[1]));
            tower.deactivate();
            superscan.getActivator(sector,tower).getBlock().setType(Material.REDSTONE_BLOCK);
            commandSender.sendMessage(ChatColor.GREEN+"Tower deactivated");
        } catch (Exception e) {
            catchException(e);
        }
        return true;
    }
}
