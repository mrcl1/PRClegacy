package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.Autoxana.Autoxana;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jack on 4/6/18.
 */
public class AnnexCommand extends CarthageCommand implements TabCompleter{

    public AnnexCommand(Main plugin) {
        super(plugin, "annex");
        this.setPermission("ProjectCarthage.annex");
        this.setHelpMessage(String.format(ChatColor.YELLOW+"usage: /annex <subcommand> \n" +
                "Use the power of the supercomputer to affect the real world\n" +
                "Subcommands:\n %s","activatorlist \n activatorget \n activatoradd <sector> <tower> \n activate <sector> <tower>\n autoxana <arg>\n "+ChatColor.RED+ChatColor.MAGIC+"doomsday"));
        this.addSubCommand(new ActivatorAdd(plugin));
        this.addSubCommand(new ActivatorList(plugin));
        this.addSubCommand(new Activate(plugin));
        this.addSubCommand(new Deactivate(plugin));
        this.addSubCommand(new RandomXana(plugin));
        this.addSubCommand(new Autoxana(plugin));
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try{
            if (commandSender instanceof Player){
                Checker.checkPerm((Player) commandSender,getPermission());
            }
            Checker.checkarglength(strings,1);
            checkSubCommands(strings[0]);

        }catch (Exception e){
            catchException(e);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        List<String> tabcomplete = new ArrayList<>();
        tabcomplete.add("activatoradd");
        tabcomplete.add("activatorlist");
        tabcomplete.add("activatorget");
        tabcomplete.add("activate");
        tabcomplete.add("deactivate");
        tabcomplete.add("forest");
        tabcomplete.add("desert");
        tabcomplete.add("ice");
        tabcomplete.add("mountain");
        return tabcomplete;
    }
}
