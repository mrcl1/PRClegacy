package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 4/6/18.
 */
public class Activate extends CarthageCommand{
    public Activate(Main plugin) {
        super(plugin, "activate");
        this.setPermission("ProjectCarthage.annex.activate");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /anex activate <Sector> <Tower> \n" +
                "Activate a tower\nAliasses:\n act");
        this.addAlias("act");
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            Checker.checkarglength(strings,2);
            Sector sector = lyoko.findSector(strings[0]);
            Tower tower = sector.findTower(Integer.parseInt(strings[1]));
            tower.activate();
            superscan.toggleTower(sector,tower);
            commandSender.sendMessage(ChatColor.GREEN+"Tower activated");

        } catch (Exception e) {
            catchException(e);
        }
        return true;
    }


}
