package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 4/6/18.
 */
public class ActivatorGet extends CarthageCommand {
    public ActivatorGet(Main plugin) {
        super(plugin, "activatorget");
        this.setPermission("ProjectCarthage.annex.activatorget");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /annex activatorget <Sector> <Tower> \n" +
                "Get details on a specific activator");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try{
            Checker.checkPerm(commandSender,getPermission());
            Checker.checkarglength(strings,2,2);
            Sector sector = superscan.getLyoko().findSector(strings[0]);
            Tower tower = sector.findTower(Integer.parseInt(strings[1]));
            commandSender.sendMessage(superscan.getActivator(sector,tower).toString());
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
