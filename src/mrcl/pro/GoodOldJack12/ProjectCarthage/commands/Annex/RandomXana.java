package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 4/8/18.
 */
public class RandomXana extends CarthageCommand{
    String[] passwords = {"Y"};
    public RandomXana(Main plugin) {
        super(plugin, "randomxana");
        this.setPermission("ProjectCarthage.annex.random");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /annex randomxana [Y] OR [sector]\n" +
                "\nAliasses:\n rx\n xana");
        this.addAlias("xana");
        this.addAlias("rx");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            Checker.checkarglength(strings,1);
            Tower tower;
            Sector sector;
            try {
                try{
                    Checker.checkArgSanity(strings[0],passwords);
                    tower = superscan.getLyoko().getRandomTower();
                }catch (Checker.InvalidArgumentException e){
                    sector = superscan.getLyoko().findSector(strings[0]);
                    tower = superscan.getLyoko().getRandomTower(sector);
                }
                superscan.toggleTower(tower.getSector(),tower);
                commandSender.sendMessage(ChatColor.GREEN+tower.toString()+" Activated");
            }catch (Superscan.InteractorDoesNotExistException e){
                onCommand(commandSender,command,s,strings);
            }

        } catch (Exception e) {
            catchException(e);
        }
        return true;
    }
}
