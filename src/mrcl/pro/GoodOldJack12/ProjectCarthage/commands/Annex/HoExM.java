package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 4/7/18.
 */
public class HoExM extends CarthageCommand {
    String[] passwords = {"Idontwanttotalkaboutit"};
    public HoExM(Main plugin) {
        super(plugin, "HoExM");
        this.setPermission("ProjectCarthage.exmachina");
        this.setHelpMessage(ChatColor.YELLOW+"Hopper saving the day again!");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            Checker.checkarglength(strings,1);
            Checker.checkArgSanity(strings[0],passwords);
            for (Tower tower : superscan.getActiveTowers()) {
                superscan.getDetector(tower.getSector(), tower).getBlock().setType(Material.REDSTONE_BLOCK);
            }
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
