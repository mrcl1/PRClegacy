package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Targeter;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by jack on 4/6/18.
 */
public class ActivatorAdd extends CarthageCommand{

    public ActivatorAdd(Main plugin) {
        super(plugin, "activatoradd");
        this.setPermission("ProjectCarthage.annex.activatoradd");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /anex activatoradd <Sector> <Tower> \n" +
                "Add an activator for a tower\nAliasses:\n aadd");
        this.addAlias("aadd");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try{
            Checker.checkPlayer(commandSender);
            Checker.checkPerm((Player)commandSender,getPermission());
            Checker.checkarglength(strings,2);
            Sector sector = superscan.getLyoko().findSector(strings[0]);
            Tower tower = sector.findTower(Integer.parseInt(strings[1]));
            Block block = Targeter.getTargetblock((Player) commandSender);
            superscan.addActivator(sector,tower,block);
            commandSender.sendMessage(ChatColor.GREEN+"Activator added as: ");
            commandSender.sendMessage(superscan.getActivator(block).toString());

        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}

