package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Activator;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * Created by jack on 4/6/18.
 */
public class ActivatorList extends CarthageCommand {
    public ActivatorList(Main plugin) {
        super(plugin, "activatorlist");
        this.setPermission("ProjectCarthage.annex.activatorlist");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /annex activatorlist\n" +
                "Get a list of all activators\nAliasses:\n ali");
        this.addAlias("ali");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);

        List<Activator> activators = superscan.getActivators();

        if (activators.isEmpty()){
            commandSender.sendMessage(ChatColor.YELLOW+"there are no activators yet");
        }else {
            commandSender.sendMessage(ChatColor.YELLOW+"current activators: ");
            for (Activator a:activators) {
                commandSender.sendMessage(" "+a.toString());
            }
        }
        return true;
    }
}
