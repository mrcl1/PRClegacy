package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.Autoxana;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 5/2/18.
 */
public class StopAutoXana extends CarthageCommand {
    public StopAutoXana(Main plugin) {
        super(plugin, "stop");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /annex autoxana stop");
        this.setPermission("ProjectCarthage.autoxana.stop");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            lyoko.getXana().stopAutoxana();
            if (!lyoko.getXana().getAutoXANA().isActivated()){
                commandSender.sendMessage(ChatColor.GREEN+"Autoxana has been stopped");
            }else {
                commandSender.sendMessage(ChatColor.RED+"Autoxana hasnt been stopped!");
            }
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
