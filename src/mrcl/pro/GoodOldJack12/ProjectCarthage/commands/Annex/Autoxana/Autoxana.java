package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.Autoxana;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 5/2/18.
 */
public class Autoxana extends CarthageCommand {
    public Autoxana(Main plugin) {
        super(plugin, "autoxana");
        this.setHelpMessage(ChatColor.YELLOW+"Wakes up xana once every 15 mins\n" +
                "Subcommands:\n" +
                " start\n stop\n status");
        this.setPermission("ProjectCarthage.autoxana");
        this.addSubCommand(new StartAutoxana(plugin));
        this.addSubCommand(new StopAutoXana(plugin));
        this.addSubCommand(new StatusAutoxana(plugin));
        this.addSubCommand(new DebugAutoXana(plugin));
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender, command, s, strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            Checker.checkarglength(strings,1,1);
            checkSubCommands(strings[0]);
        } catch (Exception e) {
            catchException(e);
        }
        return true;
    }
}
