package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.Autoxana;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Xana.XANA;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 5/2/18.
 */
public class StartAutoxana extends CarthageCommand {
    public StartAutoxana(Main plugin) {
        super(plugin, "start");
        this.setPermission("ProjectCarthage.autoxana.start");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /annex autoxana start");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try{
            Checker.checkPerm(commandSender,getPermission());
            lyoko.getXana().startAutoxana();
            if (lyoko.getXana().getAutoXANA().isActivated()){
                commandSender.sendMessage(ChatColor.GREEN+"Autoxana activated");
            }else {
                commandSender.sendMessage(ChatColor.RED+"Something went wrong, autoxana not active!");
            }
        } catch (XANA.TaskAlreadActivatedException e) {
            commandSender.sendMessage(ChatColor.RED+"Autoxana is already active!");
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
