package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.Autoxana;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jack on 5/4/18.
 */
public class DebugAutoXana extends CarthageCommand {
    public DebugAutoXana(Main plugin) {
        super(plugin, "debug");
        this.setPermission("ProjectCarthage.autoxana.debug");
        this.setHelpMessage(ChatColor.YELLOW+"Returns debug for autoxana");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender, command, s, strings);
        try {
            Checker.checkPerm(commandSender,getPermission());
            HashMap<String,List<Player>> debug = lyoko.getXana().getAutoXANA().getDebug();
            List<Player> players = debug.get("players");
            int playercount = players.size();
            List<Player> ignoredPlayers = debug.get("ignoredplayers");

            commandSender.sendMessage(ChatColor.BOLD+(ChatColor.WHITE+"Autoxana debug:"));
            commandSender.sendMessage(ChatColor.WHITE+(String.format("Counted players: %s %d",ChatColor.BLUE,playercount)));
            for (Player pl :players) {
                commandSender.sendMessage(ChatColor.GRAY+pl.getName());
            }
            commandSender.sendMessage("Currently ignoring these players:");
            for (Player pl :ignoredPlayers) {
                commandSender.sendMessage(ChatColor.GRAY+pl.getName());
            }
        } catch (Exception e) {
            catchException(e);
        }
        return true;
    }
}
