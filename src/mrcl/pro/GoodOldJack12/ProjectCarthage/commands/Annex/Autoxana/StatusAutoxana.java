package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.Autoxana;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by jack on 5/2/18.
 */
public class StatusAutoxana extends CarthageCommand {
    public StatusAutoxana(Main plugin) {
        super(plugin, "status");
        this.setPermission("ProjectCarthage.autoxana.status");
        this.setHelpMessage(ChatColor.YELLOW+"Gets the status of autoxana");

    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try{
            Checker.checkPerm(commandSender,getPermission());
            if(lyoko.getXana().getAutoXANA().isActivated()){
                commandSender.sendMessage(ChatColor.YELLOW+"Autoxana is: "+ChatColor.GREEN+"ACTIVE");
            }else {
                commandSender.sendMessage(ChatColor.YELLOW+"Autoxana is: "+ChatColor.RED+"NOT ACTIVE");

            }
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
