package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.deprecated;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Formatter;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Targeter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class deprecatedtarget implements CommandExecutor {
    private final Plugin plugin;
    private final String permission;
    private Checker checker = new Checker();
    private Targeter targeter = new Targeter();
    private Formatter formatter = new Formatter();
    public deprecatedtarget(Plugin plugin) {
        this.plugin = plugin;
        permission = String.format("%s.%s",plugin.getName(),"deprecatedtarget");
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        try {
            if (commandSender instanceof Player) {
                Checker.checkPerm((Player) commandSender, permission);
                Block targetblock = Targeter.getTargetblock((Player)commandSender);
                Material mat = targetblock.getType();
                commandSender.sendMessage(String.format("You are looking at %s on the location: %s",mat.toString(), Formatter.LocationFormat(targetblock.getLocation())));
            }else {
                commandSender.sendMessage("You can only run this as a player!");
                return true;
            }
        }catch (Checker.NoPermissionException e) {
            commandSender.sendMessage(ChatColor.RED+String.format("You lack the %s permission",permission));
            return false;
        }
        return false;
    }
}
