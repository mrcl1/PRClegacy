package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.deprecated;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Lyoko;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Detector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Targeter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class deprecatedScCommand implements CommandExecutor {
    private final Plugin plugin;
    private Checker checker = new Checker();
    private final String permission;
    private final Superscan superscan;
    Targeter targeter = new Targeter();
    private String[] allowedArgs = {"detectoradd","detectorget","Detectorlist","scan","interactordel","activatoradd"};


    public deprecatedScCommand(Plugin plugin, Superscan superscan) {
        this.plugin = plugin;
        permission = String.format("%s.%s", plugin.getName(), "superscan");
        this.superscan = superscan;

    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        try {
            if (commandSender instanceof Player) {
                Checker.checkPerm((Player) commandSender, permission);
            }
            Checker.checkarglength(strings, 1);
            String subcommand = strings[0];
            Checker.checkArgSanity(subcommand, allowedArgs);
            String[] subargs = new String[100];
            for (int i = 1; i < strings.length; i++) {
                subargs[i-1] = strings[i];
            }
            switch (subcommand) {
                case "detectoradd":
                    detectorAdd(commandSender, subargs);
                    return true;
                case "detectorget":
                    detectorGet(commandSender, subargs);
                    return true;
                case "Detectorlist":
                    detectorList(commandSender);
                    return true;
                case "scan":
                    scan(commandSender);
                    return true;
                case "interactordel":
                    interactorRemove(commandSender,subargs);
                    return true;
                case "activatoradd":
                    activatorAdd(commandSender,subargs);
                    return true;
            }
            return false;

        } catch (Checker.NoPermissionException e) {
            commandSender.sendMessage(ChatColor.RED + String.format("You lack the %s permission", permission));
            return true;
        } catch (Checker.TooLittleArgsException e) {
            commandSender.sendMessage(ChatColor.RED + "Not enough arguments!");
            return false;
        } catch (Checker.InvalidArgumentException e) {
            commandSender.sendMessage(ChatColor.RED + "Invalid arguments!");
            return false;
        }

    }

    private void detectorAdd(CommandSender commandSender, String[] args) {
        String subPerm = permission + ".detectorAdd";

        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "You must run this as a player");
        } else {
            try {
                Checker.checkPerm((Player) commandSender, subPerm);
                Sector sector = superscan.getLyoko().findSector(args[0]);
                Tower tower = sector.findTower(Integer.parseInt(args[1]));
                Block block = Targeter.getTargetblock((Player) commandSender);
                if (block.getType() == Material.REDSTONE_LAMP_OFF){
                    superscan.addDetector(sector, tower, block);
                    commandSender.sendMessage(ChatColor.GREEN+"Detector added sucessfully as:");
                    commandSender.sendMessage(superscan.getDetector(sector,tower).toString());
                }else {
                    commandSender.sendMessage(ChatColor.RED+"Detector must be a redstone lamp");
                }

            } catch (Checker.NoPermissionException e) {
                commandSender.sendMessage(ChatColor.RED + String.format("You lack the %s permission", subPerm));

           /* } catch (Checker.TooMuchArgsException e) {
                commandSender.sendMessage(ChatColor.RED + "Too much arguments!");
                commandSender.sendMessage(ChatColor.WHITE + "Usage:");
                commandSender.sendMessage(ChatColor.WHITE + "/sc detectoradd <Sector> <Tower>");

            } catch (Checker.TooLittleArgsException e) {
                commandSender.sendMessage(ChatColor.RED + "Not enough arguments!");
                commandSender.sendMessage(ChatColor.WHITE + "Usage:");
                commandSender.sendMessage(ChatColor.WHITE + "/sc detectoradd <Sector> <Tower>");*/

            } catch (Lyoko.SectorDoesNotExistException e) {
                commandSender.sendMessage(ChatColor.RED + String.format("The sector %s does not exist!", ChatColor.YELLOW + args[0] + ChatColor.RED));

            } catch (Sector.TowerDoesntExistException e) {
                commandSender.sendMessage(ChatColor.RED + String.format("tower %s does not exist in the %s sector!", ChatColor.YELLOW + args[1] + ChatColor.RED, ChatColor.YELLOW + args[0] + ChatColor.RED));

            } catch (Superscan.InteractorAlreadyExistsException e) {
                commandSender.sendMessage(ChatColor.RED + String.format("The detector already exists!"));
            } catch (Superscan.InteractorDoesNotExistException e) {
                commandSender.sendMessage(ChatColor.RED+"something went wrong getting the created detector");
            }

        }
    }

    private void detectorGet(CommandSender commandSender, String[] args){
        String subPerm = permission + ".detectorGet";
        try {
            if(commandSender instanceof Player){
                Checker.checkPerm((Player) commandSender,subPerm);
            }
            Sector sector = superscan.getLyoko().findSector(args[0]);
            Tower tower = sector.findTower(Integer.parseInt(args[1]));
            commandSender.sendMessage(superscan.getDetector(sector,tower).toString());
        } catch (Checker.NoPermissionException e) {
            commandSender.sendMessage(ChatColor.RED + String.format("You lack the %s permission", subPerm));
       /* } catch (Checker.TooMuchArgsException e) {
            commandSender.sendMessage(ChatColor.RED + "Too much arguments!");
            commandSender.sendMessage(ChatColor.WHITE + "Usage:");
            commandSender.sendMessage(ChatColor.WHITE + "/sc detectorget <Sector> <Tower>");
        } catch (Checker.TooLittleArgsException e) {
            commandSender.sendMessage(ChatColor.RED + "Not enough arguments!");
            commandSender.sendMessage(ChatColor.WHITE + "Usage:");
            commandSender.sendMessage(ChatColor.WHITE + "/sc detectorget <Sector> <Tower>");*/
        } catch (Lyoko.SectorDoesNotExistException e) {
            commandSender.sendMessage(ChatColor.RED + String.format("The sector %s does not exist!", ChatColor.YELLOW + args[0] + ChatColor.RED));
        } catch (Sector.TowerDoesntExistException e) {
            commandSender.sendMessage(ChatColor.RED + String.format("tower %s does not exist in the %s sector!", ChatColor.YELLOW + args[1] + ChatColor.RED, ChatColor.YELLOW + args[0] + ChatColor.RED));
        } catch (Superscan.InteractorDoesNotExistException e) {
            commandSender.sendMessage(ChatColor.RED + String.format("The detector for this tower does not exist"));
        }
    }

    private void interactorRemove(CommandSender commandSender, String[] args){
        String subPerm = permission + ".detectorRemove";
        if (commandSender instanceof Player) {
            try {
                Checker.checkPerm((Player) commandSender, subPerm);
                Block block = Targeter.getTargetblock((Player) commandSender);
                commandSender.sendMessage(ChatColor.YELLOW+"removing: "+ superscan.getInteractor(block).toString());
                superscan.removeInteractor(block);
            } catch (Checker.NoPermissionException e) {
                commandSender.sendMessage(ChatColor.RED + String.format("You lack the %s permission", subPerm));
            } catch (Superscan.InteractorDoesNotExistException e) {
                commandSender.sendMessage(ChatColor.RED + "This block isnt an interactor");
            }
        }else {
            commandSender.sendMessage(ChatColor.RED+"You must run this as a player!");
        }
    }

    private void detectorList(CommandSender commandSender){
        List<Detector> detectors = superscan.getDetectors();

        if (detectors.isEmpty()){
            commandSender.sendMessage(ChatColor.YELLOW+"there are no detectors yet");
        }else {
            commandSender.sendMessage(ChatColor.YELLOW+"current detectors:");
            for (Detector de : detectors) {
                commandSender.sendMessage(de.toString());
            }
        }
    }

    private void activatorAdd(CommandSender commandSender, String[] args){
        String subPerm = permission + ".activatorAdd";
        if (commandSender instanceof Player){
            try {
                Checker.checkPerm((Player) commandSender,subPerm);
                Block block = Targeter.getTargetblock((Player) commandSender);
                Sector sector = superscan.getLyoko().findSector(args[0]);
            } catch (Checker.NoPermissionException e) {
                commandSender.sendMessage(ChatColor.RED+String.format("You lack the %s permission",subPerm));
            } catch (Lyoko.SectorDoesNotExistException e) {
                commandSender.sendMessage(ChatColor.RED+String.format("this sector doesnt exist!"));
            }
        }else {
            commandSender.sendMessage(ChatColor.RED+"You must run this as a player!");
        }

    }

    private void scan(CommandSender commandSender){
        List<Detector> detectors = superscan.getDetectors();
        List<Detector> activeDetectors = new ArrayList<Detector>();
        for (Detector detector: detectors) {
            if (detector.getBlock().getType() == Material.REDSTONE_LAMP_ON){
                activeDetectors.add(detector);
            }
        }
        if (activeDetectors.isEmpty()){
            commandSender.sendMessage(ChatColor.YELLOW+"There are no active Towers");
        }else {
            commandSender.sendMessage(ChatColor.YELLOW+"---Superscan---");
            for (Detector detector:activeDetectors) {
                commandSender.sendMessage("-"+detector.getSector().getName()+" #"+detector.getTower().getNumber());
            }
        }

    }

}
