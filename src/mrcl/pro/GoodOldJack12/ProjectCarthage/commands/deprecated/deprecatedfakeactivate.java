package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.deprecated;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class deprecatedfakeactivate implements CommandExecutor{
    private final Plugin plugin;
    Checker checker = new Checker();
    private final String permission;
    public deprecatedfakeactivate(Plugin plugin) {
        this.plugin = plugin;
         permission = String.format("%s.%s",plugin.getName(),"deprecatedfakeactivate");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
            try {
                if(commandSender instanceof Player) {
                    Checker.checkPerm((Player) commandSender, permission);
                }
                Checker.checkarglength(strings,2,3);

                String tower = strings[0];
                String number = strings[1];
                char activator = 'X';
                if(strings.length > 2) {
                    activator = strings[2].charAt(0);
                    String[] allowedActivators = {"X","J","H",""};
                    Checker.checkArgSanity(strings[2],allowedActivators);
                }
                activateTower(tower, number,activator);
                return true;
            } catch (Checker.TooLittleArgsException ex) {
                commandSender.sendMessage(ChatColor.RED+"Not enough arguments!");
                return false;
            } catch (Checker.TooMuchArgsException e) {
                commandSender.sendMessage(ChatColor.RED+"too many arguments!");
                return false;
            } catch (Checker.NoPermissionException e) {
                commandSender.sendMessage(ChatColor.RED+String.format("You lack the %s permission",permission));
                return false;
            } catch (Checker.InvalidArgumentException e) {
                commandSender.sendMessage(ChatColor.RED+String.format("not a valid activator!"));
            }
            return false;
    }



    private void activateTower(String sector, String number, char activator){
        String superscanString = ChatColor.DARK_GREEN+"Superscan"+ChatColor.WHITE;
        String activatorString = ChatColor.RED+"XANA";
        switch (activator){
            case 'J': activatorString = ChatColor.AQUA+"Jeremie"; break;
            case 'H': activatorString = ChatColor.BLUE+"Hopper"; break;
        }
        plugin.getServer().broadcastMessage(String.format("%s: %s #%s was activated by %s!",superscanString,sector,number,activatorString));
    }

}
