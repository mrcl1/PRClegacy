package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Lyoko;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class CarthageCommand implements CommandExecutor {
    //utilities
    protected JavaPlugin plugin;

    //plugin essentials
    protected Lyoko lyoko;
    protected Superscan superscan;
    private String permission;
    private List<CarthageCommand> subcommands = new ArrayList<>();
    private List<String> allowedArgs = new ArrayList<>();
    private String HelpMessage;
    private String name;
    private List<String> aliases = new ArrayList<>();

    //onCommand arguments
    protected CommandSender commandSender;
    protected Command command;
    protected String s;
    protected String[] strings;

    //default constructor
    public CarthageCommand(Main plugin, String name){
        this.plugin = plugin;
        this.setupCommand(name);
        this.lyoko = plugin.getLyoko();
        this.superscan = lyoko.getSuperscan();
    }

    //getters
    public String getPermission() {
        return permission;
    }
    public String getHelpMessage() {
        return HelpMessage;
    }
    public String getName() {return name;}
    public List<String> getAllowedArgs() {
        return allowedArgs;
    }
    public List<CarthageCommand> getSubcommands() {
        return subcommands;
    }

    //setters
    public void setHelpMessage(String helpMessage){
        this.HelpMessage = helpMessage;
    }
    public void setPermission(String permission) {
        this.permission = permission;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSubcommands(List<CarthageCommand> subcommands) {
        this.subcommands = subcommands;
    }
    public void setAllowedArgs(List<String> allowedArgs) {
        this.allowedArgs = allowedArgs;
    }


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender, command,  s, strings);
        return false;
    }
    /**Registers all the arguments of onCommand for usage in methods outside of it**/
    protected void registerSender(CommandSender commandSender, Command command, String s, String[] strings){
        this.commandSender = commandSender;
        this.command = command;
        this.s = s;
        this.strings = strings;
    }
    /**Sets up help messages, permissions and so forth according to a default procedure. should be overrided in most cases**/
    protected void setupCommand(String name){
        this.setName(name);
        getAllowedArgs().add("3546547665766876486764135789876"); //random argument as default
    }
    //subcommands
    protected void addSubCommand(CarthageCommand command){
        this.subcommands.add(command);
    }
    protected boolean checkSubCommands(String arg) throws Checker.InvalidArgumentException {
        boolean itsAnOlderCommandButItChecksOut = false;
        for (CarthageCommand command:subcommands) {
            if (command.getName().equals(arg) || command.getAliases().contains(arg)){
                itsAnOlderCommandButItChecksOut = true;
                List<String> args = new ArrayList<>();
                args.addAll(Arrays.asList(strings));
                args.remove(0);
                String[] newStrings = args.toArray(new String[0]);

                command.onCommand(this.commandSender,this.command,this.s,newStrings);
            }
        }
        if (!itsAnOlderCommandButItChecksOut){
            throw new Checker.InvalidArgumentException();
        }
        return itsAnOlderCommandButItChecksOut;

    }
    public List<String> getAliases(){
        return aliases;
    }
    protected void addAlias(String string){
        this.aliases.add(string);
    }

    /**Catches some common exceptions (actually basically all of them) that most commands throw and calls the appropriate method to send an error message**/
    protected void catchException(Exception e){
        if (e.getClass().equals(Checker.NoPermissionException.class)){
            catchPermdenied();
        }else if(e.getClass().equals(Checker.InvalidArgumentException.class)){
            catchInvalidArgument();
        }else if(e.getClass().equals(Checker.TooLittleArgsException.class)) {
            catchTooLittleArguments();
        }else if(e.getClass().equals(Checker.TooMuchArgsException.class)){
            catchTooMuchArguments();
        }else if(e.getClass().equals(Lyoko.SectorDoesNotExistException.class)){
            catchSectorDoesntExist();
        }else if(e.getClass().equals(Sector.TowerDoesntExistException.class)){
            catchTowerDoesntExist();
        }else if(e.getClass().equals(Tower.AlreadyActivatedException.class)){
            catchTowerActivated();
        }else if(e.getClass().equals(Tower.AlreadyDeactivatedException.class)){
            catchTowerDeactivated();
        }else if(e.getClass().equals(Checker.NotAPlayerException.class)) {
            catchnotAPlayer();
        }else if(e.getClass().equals(Superscan.InteractorAlreadyExistsException.class)) {
            catchInteractorExists();
        }else if(e.getClass().equals(Superscan.InteractorDoesNotExistException.class)){
            catchInteractorDoesntExist();
        }else {
            commandSender.sendMessage(ChatColor.RED+"something went wrong! check the log!");
            Bukkit.getLogger().warning(e.getClass().toString()+ e.getMessage());
        }
    }


    //default error messages
    private void catchPermdenied(){
        commandSender.sendMessage(String.format("You lack the %s permission",this.permission));
    }
    private void catchInvalidArgument(){
        commandSender.sendMessage(String.format(ChatColor.RED+"Invalid arguments!"));
        commandSender.sendMessage(getHelpMessage());
    }
    private void catchTooLittleArguments(){
        commandSender.sendMessage(String.format(ChatColor.RED+"Not enough arguments!"));
        commandSender.sendMessage(getHelpMessage());
    }
    private void catchTooMuchArguments(){
        commandSender.sendMessage(ChatColor.RED+"Too much arguments!");
        commandSender.sendMessage(getHelpMessage());
    }
    private void catchSectorDoesntExist(){
        commandSender.sendMessage(ChatColor.RED+"This sector doesnt exist");
    }
    private void catchTowerDoesntExist(){
        commandSender.sendMessage(ChatColor.RED+"This tower doesnt exist");
    }
    private void catchTowerActivated(){
        commandSender.sendMessage(ChatColor.RED+"This tower is already activated");
    }
    private void catchTowerDeactivated(){
        commandSender.sendMessage(ChatColor.RED+"This tower is already deactivated");
    }
    private void catchnotAPlayer(){
        commandSender.sendMessage(ChatColor.RED+"You must run this as a player");
    }
    private void catchInteractorExists(){
        commandSender.sendMessage(ChatColor.RED+"This block is already an interactor!");
    }
    private void catchInteractorDoesntExist(){
        commandSender.sendMessage(ChatColor.RED+"This block isnt an interactor!");
    }
}
