package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.SuperScan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Targeter;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by jack on 4/6/18.
 */
public class Interactordel extends CarthageCommand {
    public Interactordel(Main plugin) {
        super(plugin, "interactordel");
        this.setPermission("ProjectCarthage.superscan.interactordel");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /sc interactordel \n" +
                "Removes the interactor you're looking at\nAliasses:\n idel");
        this.addAlias("idel");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            Checker.checkPlayer(commandSender);
            Checker.checkPerm((Player) commandSender,getPermission());

            Block block = Targeter.getTargetblock((Player) commandSender);
            commandSender.sendMessage(ChatColor.YELLOW+"removing: "+ superscan.getInteractor(block).toString());
            superscan.removeInteractor(block);
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
