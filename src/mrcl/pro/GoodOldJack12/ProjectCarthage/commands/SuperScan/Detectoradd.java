package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.SuperScan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Targeter;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by jack on 4/6/18.
 */
public class Detectoradd extends CarthageCommand {
    public Detectoradd(Main plugin) {
        super(plugin, "detectoradd");
        this.setHelpMessage(ChatColor.YELLOW+"Usage: /sc detectoradd <Sector> <Tower> \n " +
                "Make the redstone lamp you're looking at a detector for the superscan\nAliasses:\n dadd");
        this.setPermission("ProjectCarthage.superscan.detectoradd");
        this.addAlias("dadd");

    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);

        try{
            Checker.checkPlayer(commandSender);
            Checker.checkPerm((Player) commandSender,getPermission());
            Checker.checkarglength(strings,2);
            Sector sector = superscan.getLyoko().findSector(strings[0]);
            Tower tower = sector.findTower(Integer.parseInt(strings[1]));
            Block block = Targeter.getTargetblock((Player) commandSender);
            if (block.getType() == Material.REDSTONE_LAMP_OFF){
                superscan.addDetector(sector, tower, block);
                commandSender.sendMessage(ChatColor.GREEN+"Detector added sucessfully as:");
                commandSender.sendMessage(superscan.getDetector(sector,tower).toString());
            }else {
                commandSender.sendMessage(ChatColor.RED+"Detector must be a redstone lamp");
            }
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
