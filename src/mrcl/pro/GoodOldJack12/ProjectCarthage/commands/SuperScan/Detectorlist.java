package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.SuperScan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Detector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * Created by jack on 4/5/18.
 */
public class Detectorlist extends CarthageCommand {
    public Detectorlist(Main plugin) {
        super(plugin, "detectorlist");
        setPermission("ProjectCarthage.superscan.Detectorlist");
        setHelpMessage(ChatColor.YELLOW+"usage: /Detectorlist \n gives a list of all detectors\nAliasses:\n dli");
        this.addAlias("dli");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);

        List<Detector> detectors = superscan.getDetectors();

        if (detectors.isEmpty()){
            commandSender.sendMessage(ChatColor.YELLOW+"there are no detectors yet");
        }else {
            commandSender.sendMessage(ChatColor.YELLOW+"current detectors:");
            for (Detector de : detectors) {
                commandSender.sendMessage(de.toString());
            }
        }
        return true;
    }
}
