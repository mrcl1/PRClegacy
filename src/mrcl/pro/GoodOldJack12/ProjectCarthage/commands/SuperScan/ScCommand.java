package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.SuperScan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jack on 4/5/18.
 */
public class ScCommand extends CarthageCommand implements TabCompleter{
    public ScCommand(Main plugin) {
        super(plugin, "superscan");
        this.setPermission("ProjectCarthage.superscan");
        this.setHelpMessage(String.format(ChatColor.YELLOW+"usage: /sc <subcommand> \n" +
                "subcommands: \n %s","scan \n detectadd \n detectlist \n interactdel"));

        this.addSubCommand(new Scan(plugin));
        this.addSubCommand(new Detectorlist(plugin));
        this.addSubCommand(new Detectoradd(plugin));
        this.addSubCommand(new Interactordel(plugin));
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try{
            if (commandSender instanceof Player){
                Checker.checkPerm((Player) commandSender,getPermission());
            }
            Checker.checkarglength(strings,1);
            checkSubCommands(strings[0]);

        }catch (Exception e){
            catchException(e);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        List<String> tabcomplete = new ArrayList<>();
        tabcomplete.add("scan");
        tabcomplete.add("detectorlist");
        tabcomplete.add("detectoradd");
        tabcomplete.add("interactordel");
        tabcomplete.add("forest");
        tabcomplete.add("desert");
        tabcomplete.add("ice");
        tabcomplete.add("mountain");
        return tabcomplete;
    }
}
