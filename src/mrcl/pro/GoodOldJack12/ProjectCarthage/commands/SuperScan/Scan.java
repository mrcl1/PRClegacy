package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.SuperScan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Scan extends CarthageCommand{
    public Scan(Main plugin) {
        super(plugin, "scan");
        this.setPermission("ProjectCarthage.superscan.scan");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /sc scan\n" +
                "scan the network for active towers");
    }
    @Override
    protected void setupCommand(String name) {
        setName(name);
        setPermission("ProjectCarthage.superscan.scan");
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);

       try {
           Checker.checkarglength(strings,0,1);
           if (strings.length>0){
               try {
                   Player player = plugin.getServer().getPlayer(strings[0]);
                   commandSender = player;
                   registerSender(commandSender,command,s,strings);
               }catch (NullPointerException e){
                   commandSender.sendMessage("That player doesnt seem to exist");
               }
           }
           List<Tower> activeTowers = superscan.getActiveTowers();
           if (activeTowers.isEmpty()){
               commandSender.sendMessage(ChatColor.GREEN+"There are no active towers");
           }else {
               commandSender.sendMessage(ChatColor.AQUA+"====Superscan====");
               commandSender.sendMessage( ChatColor.WHITE+"Active towers: "+ChatColor.RED+activeTowers.size());
               List<Tower> forestTowers = superscan.getActiveTowers(superscan.getLyoko().findSector("forest"));
               List<Tower> iceTowers = superscan.getActiveTowers(superscan.getLyoko().findSector("ice"));
               List<Tower> desertTowers = superscan.getActiveTowers(superscan.getLyoko().findSector("desert"));
               List<Tower> mountainTowers = superscan.getActiveTowers(superscan.getLyoko().findSector("mountain"));
               List<Tower> carthageTowers = superscan.getActiveTowers(superscan.getLyoko().findSector("carthage"));

               if (!carthageTowers.isEmpty()){
                   commandSender.sendMessage(ChatColor.BOLD+(ChatColor.WHITE+" Carthage:"));
                   for (Tower tower: forestTowers) {
                       commandSender.sendMessage(ChatColor.RED+String.format("  Tower %d",tower.getNumber()));
                   }
               }
               if (!forestTowers.isEmpty()){
                   commandSender.sendMessage(ChatColor.DARK_GREEN+" Forest:");
                   for (Tower tower: forestTowers) {
                       commandSender.sendMessage(ChatColor.RED+String.format("  Tower %d",tower.getNumber()));
                   }
               }
               if (!iceTowers.isEmpty()){
                   commandSender.sendMessage(ChatColor.BLUE+" Ice:");
                   for (Tower tower: iceTowers) {
                       commandSender.sendMessage(ChatColor.RED+String.format("  Tower %d",tower.getNumber()));
                   }
               }
               if (!desertTowers.isEmpty()){
                   commandSender.sendMessage(ChatColor.GOLD+" Desert:");
                   for (Tower tower: desertTowers) {
                       commandSender.sendMessage(ChatColor.RED+String.format("  Tower %d",tower.getNumber()));
                   }
               }
               if (!mountainTowers.isEmpty()){
                   commandSender.sendMessage(ChatColor.DARK_PURPLE+" Mountain:");
                   for (Tower tower: mountainTowers) {
                       commandSender.sendMessage(ChatColor.RED+String.format("  Tower %d",tower.getNumber()));
                   }
               }
               commandSender.sendMessage(ChatColor.AQUA+"=================");

           }

       }catch (Exception e){
           catchException(e);
       }
          return true;
    }



}
