package mrcl.pro.GoodOldJack12.ProjectCarthage.commands.SuperScan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Checker;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Abstract.CarthageCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by jack on 4/6/18.
 */
public class Detectorget extends CarthageCommand {
    public Detectorget(Main plugin, Superscan superscan) {
        super(plugin, "dectorget");
        this.setPermission("ProjectCarthage.superscan.detectorget");
        this.setHelpMessage(ChatColor.YELLOW+"usage: /sc detectorget <Sector> <Tower> \n" +
                "Gives the location of the detector for a specific tower");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        registerSender(commandSender,command,s,strings);
        try {
            if(commandSender instanceof Player){
                Checker.checkPerm((Player) commandSender,getPermission());
            }
            Checker.checkarglength(strings,2,2);
            Sector sector = superscan.getLyoko().findSector(strings[0]);
            Tower tower = sector.findTower(Integer.parseInt(strings[1]));
            commandSender.sendMessage(superscan.getDetector(sector,tower).toString());
        }catch (Exception e){
            catchException(e);
        }
        return true;
    }
}
