package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Exceptions;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.LyokoUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



/**
 * Created by jack on 5/2/18.
 */

public class ReturnToThePast {
    private final int cooldown = 10 * 60 * 1000; //minutes in miliseconds
    private long lastActivation = System.currentTimeMillis() - cooldown;
    private Superscan superscan;
    private HashMap<Player,Location> locations = new HashMap<>();
    private long oldTime = 0;
    private List<Player> players = new ArrayList<>();
    private List<Player> ignorePlayers = new ArrayList<>();
    private List<Player> lyokoplayers = new ArrayList<>();

    public ReturnToThePast(Superscan superscan) {
        this.superscan = superscan;
    }

    public void activate(boolean force) throws Exceptions.CooldownException, Exceptions.TowerActivated {
        players = getPlayers();
        if (force){
            doRttp();
        }else {
            if (runchecks()) {
                this.lastActivation = System.currentTimeMillis();
                doRttp();
            }else {
                throw new Exceptions.TowerActivated();
            }
        }
    }


    private void doRttp(){
        resetTime();
        doCodeEarth();
        playvfx();
        teleportPlayers();
        playsfx();
    }


    private void doCodeEarth(){
        for (Player pl: getLyokoplayers()) {
            LyokoUtil.removetags(pl);
            pl.getInventory().clear();
            for (PotionEffect effect:pl.getActivePotionEffects()) {
                pl.removePotionEffect(effect.getType());
            }
        }
    }

    private boolean runchecks() throws Exceptions.CooldownException {
        if (!superscan.getActiveTowers().isEmpty()){
            return false;
        }else {
            if ((System.currentTimeMillis() - lastActivation) < cooldown ){
                throw new Exceptions.CooldownException(Math.abs((System.currentTimeMillis() - lastActivation) - cooldown));
            }else {
                return true;
            }
        }
    }
    private void teleportPlayers(){
        for (Player pl:players) {
            if(!pl.hasPermission("ProjectCarthage.rttp.permanentignore")) {
                if(locations.get(pl) != null){
                    pl.teleport(locations.get(pl));
                }else {
                    pl.teleport(Bukkit.getWorld("newkadic").getSpawnLocation());
                }
            }
        }
    }
    private void playvfx(){
        for (Player pl:players){
            pl.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS,40,255));
            pl.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,40,255));
        }
    }
    private void playsfx(){
        for (Player pl:players) {
            pl.playSound(pl.getLocation(),"rttp",1,1);
        }
    }
    private List<Player> getPlayers(){
        List<Player> players = new ArrayList<>();
        players.addAll(Bukkit.getOnlinePlayers());
        if(!ignorePlayers.isEmpty()){
            players.removeAll(ignorePlayers);

        }
        return players;
    }
    private List<Player> getLyokoplayers(){
        List<Player> allplayers = getPlayers();
        List<Player> lyokplayers = new ArrayList<>();
        for (Player pl: allplayers) {
            String world = pl.getWorld().getName();
            if (world.equalsIgnoreCase("carthage1")|| world.equalsIgnoreCase("carthage") || world.equalsIgnoreCase("nightmarerealm") ||
                    world.equalsIgnoreCase("ice") || world.equalsIgnoreCase("mountain") || world.equalsIgnoreCase("network1")) {
                lyokplayers.add(pl);
            }
        }
        return lyokplayers;
    }
    private void resetTime(){
        try {
            Bukkit.getWorld("newkadic").setFullTime(oldTime);
        }catch (NullPointerException e){
            players.get(0).getWorld().setFullTime(oldTime);
        }
    }
    public void saveLocations(){
        saveTime();
        locations.clear();
        for (int i = 0; i < players.size(); i++) {
            Player pl = players.get(i);
            String world = pl.getWorld().getName();
            if (!getLyokoplayers().contains(pl)) {
                locations.put(pl, pl.getLocation());
            }
        }
    }

    public List<Player> getIgnorePlayers() {
        return ignorePlayers;
    }

    public void saveTime(){
        try {
            oldTime = Bukkit.getWorld("newkadic").getFullTime();
        }catch (NullPointerException e) {
            oldTime = players.get(0).getWorld().getFullTime();
        }
    }
}
