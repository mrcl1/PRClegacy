package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Xana;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Lyoko;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import org.bukkit.Bukkit;

/**
 * Created by jack on 5/1/18.
 */
public class XANA {
    private Main plugin;
    private Superscan superscan;
    private Lyoko lyoko;
    private AutoXANA autoXANA;
    private int AutoXanataskID;
    public XANA(Main plugin){
        this.plugin = plugin;
        this.lyoko = plugin.getLyoko();
        this.superscan = lyoko.getSuperscan();
        autoXANA = new AutoXANA(this.superscan);
    }
    public XANA(Main plugin, Lyoko lyoko){
        this.plugin = plugin;
        this.lyoko = lyoko;
        this.superscan = lyoko.getSuperscan();
        autoXANA = new AutoXANA(this.superscan);
    }
    public void startTasks(){
        try {
            startAutoxana();
        } catch (TaskAlreadActivatedException e) {

        }
    }



    public void startAutoxana() throws TaskAlreadActivatedException {
        if(!autoXANA.isActivated()){
            AutoXanataskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin,autoXANA,6000,18000);
            autoXANA.setActivated(true);
        }else {
            throw new TaskAlreadActivatedException();
        }
    }
    public void stopAutoxana(){
        Bukkit.getScheduler().cancelTask(AutoXanataskID);
        autoXANA.setActivated(false);
    }

    public AutoXANA getAutoXANA() {
        return autoXANA;
    }

    public class TaskAlreadActivatedException extends Exception{

    }


}
