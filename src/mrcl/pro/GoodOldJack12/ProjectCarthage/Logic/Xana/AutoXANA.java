package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Xana;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AutoXANA implements Runnable{
    String perm = "ProjectCarthage.autoxana.ignore";
    private boolean activated = false;
    private Superscan superscan;
    public AutoXANA(Superscan superscan){
        this.superscan = superscan;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public boolean isActivated() {
        return activated;
    }

    private void autoXana() {
        if (superscan.getActiveTowers().isEmpty()) {
            List<Player> players = getPlayers();
            int pc = players.size();
            if(pc !=0){
                double towdouble = (pc/4)+1;
                int tow = (int) Math.floor(towdouble);
                for (int i = 0; i < tow; i++) {
                    Tower tower = superscan.toggleRandomTower();
                    Bukkit.getLogger().info("Autoxana activated"+tower.toString());
                }
            }else {
                Bukkit.getLogger().info("Autoxana detected no lyokowarriors");
            }

        }

    }
    private List<Player> getPlayers(){
        List<Player> players = new ArrayList<>();
        players.addAll(Bukkit.getOnlinePlayers());
        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            if (player.hasPermission(perm)) {
                players.remove(i);
            }
        }
        return players;
    }

    public HashMap<String,List<Player>> getDebug(){
        HashMap<String,List<Player>> debug = new HashMap<>();
        debug.put("players",getPlayers());
        List<Player> ignoredPlayers = new ArrayList<>();
        for (Player player:Bukkit.getOnlinePlayers()) {
            if (player.hasPermission(perm)){
                ignoredPlayers.add(player);
            }
        }
        debug.put("ignoredplayers",ignoredPlayers);
        return debug;
    }

    @Override
    public void run() {
        autoXana();
    }


}
