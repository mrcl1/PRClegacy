package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util;

/**
 * Created by jack on 5/2/18.
 */
public class Exceptions {
    public static class CooldownException extends Exception{
        private long remaining;
        public CooldownException(long remaining){
            this.remaining = remaining;
        }
        public long getRemaining(){
            return remaining;
        }
    }
    public static class TowerActivated extends Exception{

    }
}
