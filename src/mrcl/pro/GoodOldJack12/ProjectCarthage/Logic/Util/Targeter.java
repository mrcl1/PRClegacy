package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class Targeter {
    private static Set<Material> transparentBlocks = new HashSet<>();
    public static Block getTargetblock(Player player) {
        transparentBlocks.add(Material.AIR);
        Block targetblock = (player).getTargetBlock(transparentBlocks,50);
        if (targetblock.getType() == Material.AIR){
            player.sendMessage(ChatColor.RED+"The block is too far away!");
        }
        return targetblock;
    }


}
