package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util;

import org.bukkit.Location;

public class Formatter {

    public static String LocationFormat(Location location){
        int xloc = location.getBlockX();
        int ylock = location.getBlockY();
        int zlock = location.getBlockZ();
        String world = location.getWorld().getName();

        return String.format("X:%d Y:%d Z:%d World:%s",xloc,ylock,zlock,world);
    }
}
