package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util;

import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jack on 5/7/18.
 */
public class LyokoUtil {
    private static List<Player> devirtedplayers = new ArrayList<>();

    public static void devirtualize(Player player){
        if(!player.isDead()){
            player.setHealth(0);
        }
        removetags(player);
        if (devirtedplayers.contains(player)){
            Bukkit.getLogger().warning("Player "+ player.getName() + " was devirtualized but had a devirtualized state already!");
        }
        devirtedplayers.add(player);
    }

    public static void codeEarth(Player player){
        player.setHealth(player.getMaxHealth());
    }
    public static void removetags(Player player){
        for (String tag:player.getScoreboardTags()) {
            player.removeScoreboardTag(tag);
        }
        try {
            Team team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("Lyoko");
            if(team.hasPlayer(player)){
                team.removePlayer(player);
            }else {
                Bukkit.getLogger().info("Player was stripped from lyoko team but wasnt on lyoko team");
            }
        }catch (NullPointerException e){}
        player.getWorld().playSound(player.getLocation(), "devirt",SoundCategory.MASTER,10,1);
    }



    public static List<Player> getDevirtedplayers() {
        return devirtedplayers;
    }
    public static List<Player> getLyokoplayers(){
        List<Player> allplayers = new ArrayList<>();
        allplayers.addAll(Bukkit.getOnlinePlayers());
        List<Player> lyokplayers = new ArrayList<>();
        for (Player pl: allplayers) {
            String world = pl.getWorld().getName();
            if (world.equalsIgnoreCase("carthage1") || world.equalsIgnoreCase("carthage")|| world.equalsIgnoreCase("nightmarerealm") ||
                    world.equalsIgnoreCase("ice") || world.equalsIgnoreCase("mountain") || world.equalsIgnoreCase("network1")) {
                lyokplayers.add(pl);
            }
        }
        return lyokplayers;
    }

    public static void returnToScanner(Player player){
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(),"warp scanners "+player.getName());
        player.playSound(player.getLocation(),"devirtscanner",1,1);
    }

}
