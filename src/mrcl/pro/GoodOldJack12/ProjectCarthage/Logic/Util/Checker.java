package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Checker {
    public static class TooLittleArgsException extends Exception{

    }
    public static class TooMuchArgsException extends Exception{

    }
    public static class NoPermissionException extends Exception{

    }

    public static class InvalidArgumentException extends Exception{

    }
    public static class NotAPlayerException extends Exception{

    }

    public static void checkarglength(String[] args, int min, int max) throws TooLittleArgsException, TooMuchArgsException {
        if(args.length < min){
            throw new TooLittleArgsException();
        }
        if(args.length > max){
           throw new TooMuchArgsException();
        }
    }

    public static void checkarglength(String[] args, int min) throws TooLittleArgsException{
        if(args.length < min){
            throw new TooLittleArgsException();
        }
    }

    public static void checkPerm(Player player, String permission) throws NoPermissionException {
        if(!(player.hasPermission(permission))){
            throw new NoPermissionException();
        }
    }
    public static void checkPerm(CommandSender commandSender, String permission) throws NoPermissionException {
        if (commandSender instanceof Player){
            Player player = (Player) commandSender;
            if(!(player.hasPermission(permission))){
                throw new NoPermissionException();
            }
        }
    }

    public static void checkArgSanity(String arg, String[] valids) throws InvalidArgumentException {
        boolean sanity = false;
        for (String valid : valids) {
            if (valid.equals(arg)) {
                sanity = true;
            }
        }
        if (!sanity){
            throw new InvalidArgumentException();
        }
    }
    public static void checkPlayer(CommandSender commandSender) throws NotAPlayerException {
        boolean player = false;
        if (commandSender instanceof Player){
            player = true;
        }
        if(!player){
            throw new NotAPlayerException();
        }
    }

}
