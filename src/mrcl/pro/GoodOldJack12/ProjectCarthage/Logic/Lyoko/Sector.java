package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko;

import java.util.ArrayList;
import java.util.List;

public class Sector {
    private String name;
    private List<Tower> towers = new ArrayList<Tower>();
    private int tottowers;

    public Sector(String name, int towers) {
        this.name = name;
        for (int i = 1; i <= towers; i++) {
            this.towers.add(new Tower(this,i));
        }
        this.tottowers = towers;
    }

    public Sector(String name) {
        this.name = name;
        for (int i = 1; i <= 10; i++) {
            this.towers.add(new Tower(this,i));
        }
        this.tottowers = 10;
    }

    public void activate(int tower) throws Tower.AlreadyActivatedException, TowerDoesntExistException {
        findTower(tower).activate();
    }
    public void deactivate(int tower) throws TowerDoesntExistException, Tower.AlreadyDeactivatedException {
        findTower(tower).deactivate();
    }

    public String getName() {
        return name;
    }

    public Tower findTower(int tower) throws TowerDoesntExistException {
        for (Tower sectower : towers) {
            if (sectower.getNumber() == tower) {
                return sectower;
            }
        }
        throw new TowerDoesntExistException();
    }

    public Tower findTower(Tower tower) throws TowerDoesntExistException{
        for (Tower sectower : towers) {
            if (sectower.equals(tower)) {
                return sectower;
            }
        }
        throw new TowerDoesntExistException();
    }

    public List<Tower> getTowers() {
        return towers;
    }

    public class TowerDoesntExistException extends Exception {

    }
}
