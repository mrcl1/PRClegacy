package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Xana.XANA;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Lyoko {
    private final Main plugin;
    private List<Sector> sectors= new ArrayList<Sector>();
    private Superscan superscan;
    private String name;
    private XANA xana;
    public Lyoko(Main plugin) {
        AddSector("forest");
        AddSector("ice");
        AddSector("desert");
        AddSector("mountain");
        AddSector("carthage",1);
        this.name = "lyoko";
        this.plugin = plugin;
        superscan = new Superscan(plugin,this);
        xana = new XANA(plugin,this);
    }

    public void AddSector(String name){
        sectors.add(new Sector(name));
    }
    public void AddSector(String name,int towers){
        sectors.add(new Sector(name,towers));
    }

    public List<Sector> getSectors() {
        return sectors;
    }

    public void activate(String sector, int tower) throws Tower.AlreadyActivatedException, Sector.TowerDoesntExistException, SectorDoesNotExistException {
        findSector(sector).findTower(tower).activate();
    }
    public void deactivate(String sector, int tower) throws SectorDoesNotExistException, Sector.TowerDoesntExistException, Tower.AlreadyDeactivatedException {
        findSector(sector).findTower(tower).deactivate();
    }


    public Sector findSector(String sector) throws SectorDoesNotExistException {
        for (Sector selSector: sectors) {
            if(selSector.getName().equals(sector)){
                return selSector;
            }
        }
        throw new SectorDoesNotExistException();
    }
    public Sector findSector(Sector sector) throws SectorDoesNotExistException{
        for (Sector selSector: sectors) {
            if(selSector.equals(sector)){
                return selSector;
            }
        }
        throw new SectorDoesNotExistException();
    }
    public List<Tower> getTowers(){
        List<Tower> towers = new ArrayList<>();
        for (Sector sector: sectors) {
            towers.addAll(sector.getTowers());
        }
        return towers;
    }
    public List<Tower> getTowers(Sector sector){
        return sector.getTowers();
    }
    public Tower getRandomTower(){
        Random random = new Random();
        return getTowers().get(random.nextInt(getTowers().size()));
    }
    public Tower getRandomTower(Sector sector){
        Random random = new Random();
        return getTowers(sector).get(random.nextInt(getTowers(sector).size()));
    }

    public XANA getXana() {
        return xana;
    }

    public class SectorDoesNotExistException extends Exception {

    }
    public String getName(){
        return name;
    }

    public Superscan getSuperscan() {
        return superscan;
    }
}
