package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko;

public class Tower{
    private boolean activated;
    private int number;
    private Sector sector;
    public Tower(Sector sector,int number) {
        this.number = number;
        activated = false;
        this.sector = sector;
    }

    public void activate() throws AlreadyActivatedException {
        if(activated){
            throw new AlreadyActivatedException();
        }else {
            activated = true;
        }
    }
    public void deactivate() throws AlreadyDeactivatedException{
        if(!activated){
            throw new AlreadyDeactivatedException();
        }else {
            activated = false;
        }
    }

    @Override
    public String toString() {
        return String.format("%s %d",getSector().getName(),getNumber());
    }

    public boolean isActivated() {
        return activated;
    }

    public class AlreadyActivatedException extends Exception{

    }

    public Sector getSector() {
        return sector;
    }

    public class AlreadyDeactivatedException extends Exception{

    }

    public int getNumber() {
        return number;
    }

    }



