package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Lyoko;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Formatter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public abstract class Interactor implements ConfigurationSerializable {
    Formatter formatter = new Formatter();
    Lyoko lyoko;
    Sector sector;
    Tower tower;
    private Block block;
    public Interactor(Lyoko lyoko, Sector sector, Tower tower, Block block) {
        this.lyoko = lyoko;
        this.sector = sector;
        this.tower = tower;
        this.block = block;
    }
    public Interactor(HashMap<String,Object> serializedInteractor,Lyoko lyoko) throws Lyoko.SectorDoesNotExistException, Sector.TowerDoesntExistException {
        this.lyoko = lyoko;
        this.sector = lyoko.findSector((String) serializedInteractor.get("sector"));
        this.tower = lyoko.findSector(this.sector).findTower((int) serializedInteractor.get("tower"));
        World world = Bukkit.getServer().getWorld((String) serializedInteractor.get("world"));
        double x = Double.parseDouble(serializedInteractor.get("x").toString());
        double y = Double.parseDouble(serializedInteractor.get("y").toString());
        double z = Double.parseDouble(serializedInteractor.get("z").toString());
        Bukkit.getLogger().info("Creating interactor as:" + world + x + y + z);
        Location location = new Location(world,x,y,z);
        this.block = location.getBlock();
    }

    public Location getLocation(){
        return block.getLocation();
    }

    @Override
    public String toString(){
        String sector = this.sector.getName();
        int tower = this.tower.getNumber();

        return String.format("for %s #%d at %s",sector,tower, Formatter.LocationFormat(getLocation()));
    }

    public Block getBlock() {
        return block;
    }

    public Sector getSector() {
        return sector;
    }

    public Tower getTower() {
        return tower;
    }
    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> mapSerializer = new HashMap<>(); // Create a map that will be used to serialize the class's fields
        mapSerializer.put("sector",this.sector.getName());
        mapSerializer.put("tower",this.tower.getNumber());
        mapSerializer.put("world",this.block.getLocation().getWorld().getName());
        mapSerializer.put("x",this.block.getX());
        mapSerializer.put("y",this.block.getY());
        mapSerializer.put("z",this.block.getZ());
        return mapSerializer;
    }
}
