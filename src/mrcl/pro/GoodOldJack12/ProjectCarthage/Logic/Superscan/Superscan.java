package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Lyoko;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.ReturnToThePast;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Main;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.bukkit.craftbukkit.libs.it.unimi.dsi.fastutil.Hash;


public class Superscan implements ConfigurationSerializable{
    private Lyoko lyoko;
    private Main plugin;
    private ReturnToThePast returnToThePast;
    private List<Activator> activators = new ArrayList<Activator>();
    private List<Detector> detectors = new ArrayList<Detector>();
    public Superscan(Main plugin, Lyoko lyoko) {
        this.plugin = plugin;
        this.lyoko = lyoko;
        returnToThePast = new ReturnToThePast(this);
    }

    public void setInteractors(HashMap<String,Object> serializedInteractors){
        activators.clear();
        detectors.clear();

        ArrayList<HashMap<String,Object>> tempActivators = (ArrayList<HashMap<String, Object>>) serializedInteractors.get("activators");
        ArrayList<HashMap<String,Object>> tempDetectors = (ArrayList<HashMap<String, Object>>) serializedInteractors.get("detectors");
        for (HashMap<String,Object> activator: tempActivators) {
            try {
                activators.add(new Activator(activator,this.lyoko));
            } catch (Lyoko.SectorDoesNotExistException e) {
                plugin.getLogger().warning("Could not deserialize activator, sector does not exist!");
            } catch (Sector.TowerDoesntExistException e) {
                plugin.getLogger().warning("Could not deserialize activator, tower does not exist!");
            }
        }
        for (HashMap<String,Object> detector: tempDetectors){
            try {
                activators.add(new Activator(detector,this.lyoko));
            } catch (Lyoko.SectorDoesNotExistException e) {
                plugin.getLogger().warning("Could not deserialize detector, sector does not exist!");
            } catch (Sector.TowerDoesntExistException e) {
                plugin.getLogger().warning("Could not deserialize activator, tower does not exist!");
            }
        }

    }
    /**
     * takes a hashmap and turns it in to detector objects (used for loading from file)
     * @param serializedDetectors
     */
    public void setDetectors(HashMap<String,Object> serializedDetectors) {
        detectors.clear();
        ArrayList<HashMap<String,Object>> tempDetectors = (ArrayList<HashMap<String, Object>>) serializedDetectors.get("detectors");
        if(tempDetectors.isEmpty()){plugin.getLogger().info("tempdetectors is empty");}
        for (HashMap<String,Object> detector: tempDetectors){
            plugin.getLogger().info("tempdetectors is not empty, looping");
            try {
                detectors.add(new Detector(detector,this.lyoko));
            } catch (Lyoko.SectorDoesNotExistException e) {
                plugin.getLogger().warning("Could not deserialize detector, sector does not exist!");
            } catch (Sector.TowerDoesntExistException e) {
                plugin.getLogger().warning("Could not deserialize activator, tower does not exist!");
            }
        }

    }

    /**
     * takes a hashmap and turns it in to activator objects (used for loading from file)
     * @param serializedActivators
     */
    public void setActivators(HashMap<String,Object> serializedActivators) {
        activators.clear();
        ArrayList<HashMap<String,Object>> tempActivators = (ArrayList<HashMap<String, Object>>) serializedActivators.get("activators");
        if(tempActivators.isEmpty()){plugin.getLogger().info("tempactivators is empty");}
        for (HashMap<String,Object> activator: tempActivators){
            try {
                activators.add(new Activator(activator,this.lyoko));
            } catch (Sector.TowerDoesntExistException e) {
                plugin.getLogger().warning("Could not deserialize activator, sector does not exist!");
            } catch (Lyoko.SectorDoesNotExistException e) {
                plugin.getLogger().warning("Could not deserialize activator, tower does not exist!");
            }
        }
    }

    /**
     * Adds an activator for a given Tower in a given Sector, using a given Block.
     * Checks if an activator already exist on the tower and/or block
     * @param sector
     * @param tower
     * @param block
     * @throws Lyoko.SectorDoesNotExistException if the given sector doesnt exist
     * @throws Sector.TowerDoesntExistException if the tower in the sector doesnt exist
     * @throws InteractorAlreadyExistsException if an activator already exists
     */

    public void addActivator(Sector sector, Tower tower, Block block) throws InteractorAlreadyExistsException, Lyoko.SectorDoesNotExistException, Sector.TowerDoesntExistException {
            testForActivator(block);
            testForActivator(sector,tower);
            lyoko.findSector(sector.getName()).findTower(tower.getNumber());
            activators.add(new Activator(lyoko,sector,tower,block));
    }

    /**
     * Adds a detector for a given Tower in a given Sector, using a given Block.
     * Checks if a detector already exist on the tower and/or block
     * @param sector
     * @param tower
     * @param block
     * @throws Lyoko.SectorDoesNotExistException if the given sector doesnt exist
     * @throws Sector.TowerDoesntExistException if the tower in the sector doesnt exist
     * @throws InteractorAlreadyExistsException if a detector already exists
     */
    public void addDetector(Sector sector, Tower tower, Block block) throws Lyoko.SectorDoesNotExistException, Sector.TowerDoesntExistException, InteractorAlreadyExistsException {
            testForDetector(block);
            testForDetector(sector,tower);
            lyoko.findSector(sector.getName()).findTower(tower.getNumber());
            detectors.add(new Detector(lyoko,sector,tower,block));
    }
    /**
     * Gets an Detector for a given Tower in a given Sector
     * @param sector
     * @param tower
     * @return the Detector for the tower
     * @throws InteractorDoesNotExistException if no Detector exists for this tower
     */
    public Detector getDetector(Sector sector, Tower tower) throws InteractorDoesNotExistException {
        for (Detector detector: detectors) {
            if (detector.getSector().getName().equals(sector.getName()) && detector.getTower().getNumber() == tower.getNumber()){
                return detector;
            }
        }
        throw new InteractorDoesNotExistException();
    }
    /**
     * Gets a Detector for a given block
     * @param block
     * @return The Detector associated with the block.
     * @throws InteractorDoesNotExistException if there's no Detector for on the block.
     */
    public Detector getDetector(Block block) throws InteractorDoesNotExistException {
        for (Detector detector: detectors){
            if(detector.getBlock().equals(block)){
                return detector;
            }

        }
        throw new InteractorDoesNotExistException();
    }

    /**
     * Gets an Activator for a given Tower in a given Sector
     * @param sector
     * @param tower
     * @return the activator for the tower
     * @throws InteractorDoesNotExistException if no activator exists for this tower
     */
    public Activator getActivator(Sector sector, Tower tower) throws InteractorDoesNotExistException {
        for (Activator activator: activators){
            if(activator.getSector().getName().equals(sector.getName()) && activator.getTower().getNumber() == tower.getNumber()){
                return activator;
            }
        }
        throw new InteractorDoesNotExistException();
    }

    /**
     * Gets an Activator for a given block
     * @param block
     * @return The activator associated with the block.
     * @throws InteractorDoesNotExistException if there's no activator for on the block.
     */
    public Activator getActivator(Block block) throws InteractorDoesNotExistException {
        for (Activator activator: activators){
            if(activator.getBlock().equals(block)){
                return activator;
            }
        }
        throw new InteractorDoesNotExistException();
    }

    public Lyoko getLyoko() {
        return lyoko;
    }

    public List<Detector> getDetectors() {
        return detectors;
    }

    public List<Activator> getActivators() {
        return activators;
    }


    /**
     * Probabbly broken, dont touch it
     * @param block
     * @return
     * @throws InteractorDoesNotExistException
     */
    public Interactor getInteractor(Block block) throws InteractorDoesNotExistException {
        Interactor interactor;
        try {
            interactor = getDetector(block);
        } catch (InteractorDoesNotExistException e) {
            interactor = getActivator(block);
        }
        return interactor;
    }
    /**
     * Probabbly broken, dont touch it
     * @param block
     * @return
     * @throws InteractorDoesNotExistException
     */
    public void removeInteractor(Block block) throws InteractorDoesNotExistException {
        Interactor interactor = getInteractor(block);
        if(interactor instanceof Detector){
            detectors.remove(interactor);
        }else if (interactor instanceof Activator){
            activators.remove(interactor);
        }
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> mapSerializer = new HashMap<>(); // Create a map that will be used to serialize the class's fields
        List<Object> tempdetectors = new ArrayList<Object>();
        for (Detector detector:getDetectors()) {
            tempdetectors.add(detector.serialize());
        }
        List<Activator> activators = getActivators();
        mapSerializer.put("detectors",tempdetectors);

        List<Object> tempactivators = new ArrayList<Object>();
        for (Activator activator: getActivators()){
            tempactivators.add(activator.serialize());
        }
        mapSerializer.put("activators",tempactivators);
        return mapSerializer;
    }

    /**
     * Refreshes the tower.activated booleans.
     * Logs an error if a detector is broken.
     */
    public void refreshTowers(){
        for (Detector detector:detectors) {
            if (detector.getBlock().getType() == Material.REDSTONE_LAMP_ON){
                try {
                    detector.getTower().activate();

                } catch (Tower.AlreadyActivatedException e) {
                    //do nothing
                }
            }else if (detector.getBlock().getType() == Material.REDSTONE_LAMP_OFF){
                try {
                    detector.getTower().deactivate();
                } catch (Tower.AlreadyDeactivatedException e) {
                    //do nothing
                }
            }else{
                plugin.getLogger().warning("The detector:  "+ detector.toString() + "Is not a redstone lamp!");
            }
        }
    }
    /**Returns all active towers in Lyoko**/
    public List<Tower> getActiveTowers(){
        refreshTowers();
        List<Tower> activeTowers = new ArrayList<>();
        for (Sector se: lyoko.getSectors()) {
            for (Tower tower: se.getTowers()) {
                if (tower.isActivated()){
                    activeTowers.add(tower);
                }
            }
        }

        return activeTowers;
    }
    /**
     * Returns all active towers in a given sector in Lyoko
     * @param sector The sector that has to be searched
     **/
    public List<Tower> getActiveTowers(Sector sector){
        refreshTowers();
        List<Tower> activeTowers = new ArrayList<>();
            for (Tower tower: sector.getTowers()) {
                if (tower.isActivated()){
                    activeTowers.add(tower);
                }
            }
        return activeTowers;
    }

    /**
     * Tests if an activator exists on a given block
     * @param block
     * @throws InteractorAlreadyExistsException if an activator exists
     */
    public void testForActivator(Block block) throws InteractorAlreadyExistsException {
        for (Activator activator:activators) {
            if (activator.getBlock().equals(block)){
                throw new InteractorAlreadyExistsException();
            }
        }
    }

    /**
     * Tests if a detector exists on a given block
     * @param block
     * @throws InteractorAlreadyExistsException if a detector exists
     */
    public void testForDetector(Block block) throws InteractorAlreadyExistsException {
        for (Detector detector:detectors) {
            if (detector.getBlock().equals(block)){
                throw new InteractorAlreadyExistsException();
            }
        }
    }

    /**
     * Tests if an activator exists for a given tower in a given sector
     * @param sector
     * @param tower
     * @throws InteractorAlreadyExistsException if an activator exists
     */
    public void testForActivator(Sector sector,Tower tower) throws InteractorAlreadyExistsException {

        for (Activator activator:activators) {
            if (activator.getSector().equals(sector) && activator.getTower().equals(tower)){
                throw new InteractorAlreadyExistsException();
            }
        }
    }
    /**
     * Tests if a detector exists for a given tower in a given sector
     * @param sector
     * @param tower
     * @throws InteractorAlreadyExistsException if an activator exists
     */
    public void testForDetector(Sector sector, Tower tower) throws InteractorAlreadyExistsException {
        for (Detector detector:detectors) {
            if (detector.getSector().equals(sector) && detector.getTower().equals(tower)){
                throw new InteractorAlreadyExistsException();
            }
        }
    }

    /**
     * Toggles a tower on or off. Does not check wether or not it is on, it just places a redstone block at
     * the activator.
     * @param sector
     * @param tower
     * @throws InteractorDoesNotExistException if no activator was found for the tower
     */
    public void toggleTower(Sector sector,Tower tower) throws InteractorDoesNotExistException {
        returnToThePast.saveLocations();
        getActivator(sector,tower).getBlock().setType(Material.REDSTONE_BLOCK);
    }
    /**
     * Toggles a random tower on or off. Does not check wether or not it is on, it just places a redstone block at
     * the activator.
     * @throws InteractorDoesNotExistException if no activator was found for the tower
     */
    public Tower toggleRandomTower() {
        try {
            Tower tower = lyoko.getRandomTower();
            returnToThePast.saveLocations();

            getActivator(tower.getSector(),tower).getBlock().setType(Material.REDSTONE_BLOCK);
            return tower;
        } catch (InteractorDoesNotExistException ex) {
            return toggleRandomTower();
        }
    }

    /**
     * Toggles a random tower in a given sector on or off. Does not check wether or not it is on, it just places a redstone block at
     * the activator.
     * @param sector
     * @throws InteractorDoesNotExistException if no activator was found for the tower
     */
    public Tower toggleRandomTower(Sector sector) throws InteractorDoesNotExistException {
        Tower tower = lyoko.getRandomTower(sector);
        returnToThePast.saveLocations();
        getActivator(tower.getSector(),tower).getBlock().setType(Material.REDSTONE_BLOCK);
        return tower;
    }
    public ReturnToThePast getReturnToThePast(){
        return returnToThePast;
    }
    public class InteractorDoesNotExistException extends Exception{

    }
    public class InteractorAlreadyExistsException extends Exception{

    }


    public Main getPlugin() {
        return plugin;
    }
}
