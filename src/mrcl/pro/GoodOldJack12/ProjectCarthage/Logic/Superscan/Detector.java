package mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Lyoko;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Sector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Tower;
import org.bukkit.block.Block;

import java.util.HashMap;

public class Detector extends Interactor{

    public Detector(Lyoko lyoko, Sector sector, Tower tower, Block block) {
        super(lyoko, sector, tower, block);
    }

    public Detector(HashMap<String, Object> serializedInteractor, Lyoko lyoko) throws Lyoko.SectorDoesNotExistException, Sector.TowerDoesntExistException {
        super(serializedInteractor, lyoko);
    }

    @Override
    public String toString() {
        return "Detector "+super.toString();
    }


}
