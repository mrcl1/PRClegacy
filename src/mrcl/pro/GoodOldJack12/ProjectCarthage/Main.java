package mrcl.pro.GoodOldJack12.ProjectCarthage;

import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Lyoko.Lyoko;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Activator;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Detector;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Superscan.Superscan;
import mrcl.pro.GoodOldJack12.ProjectCarthage.Logic.Util.Formatter;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.AnnexCommand;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.Doomsday;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.Annex.HoExM;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.MiscCommands.fakeactivate;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.MiscCommands.fakedeactivate;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.MiscCommands.target;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.ReturnToThePast.ReturnToThePastCommand;
import mrcl.pro.GoodOldJack12.ProjectCarthage.commands.SuperScan.ScCommand;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;

public class Main extends JavaPlugin {
    Lyoko lyoko = new Lyoko(this);
    Formatter formatter = new Formatter();

    public static void main(String[] args) {

    }
    @Override
    public void onEnable() {
        getLogger().info("Project Carthage has booted");
        ConfigurationSerialization.registerClass(Superscan.class);
        ConfigurationSerialization.registerClass(Activator.class);
        ConfigurationSerialization.registerClass(Detector.class);
        registerCommands();
        registerEvents();
        createConfig();
        loadConfig();
        lyoko.getXana().startTasks();


    }

    private void registerCommands(){
        //deprecatedfakeactivate
        this.getCommand("fakeactivate").setExecutor(new fakeactivate(this));
        this.getCommand("fakedeactivate").setExecutor(new fakedeactivate(this));
        //deprecatedtarget
        this.getCommand("target").setExecutor(new target(this));
        //sc
        this.getCommand("sc").setExecutor(new ScCommand(this));
        this.getCommand("annex").setExecutor(new AnnexCommand(this));
        this.getCommand("doomsday").setExecutor(new Doomsday(this));
        this.getCommand("hoexm").setExecutor(new HoExM(this));
        this.getCommand("rttp").setExecutor(new ReturnToThePastCommand(this));
    }
    private void registerEvents(){
        //main event handler
        getServer().getPluginManager().registerEvents(new Listener(),this);
    }

    @Override
    public void onDisable() {
        storeConfig();
        super.onDisable();
    }

    public Lyoko getLyoko() {
        return lyoko;
    }

    public void loadConfig(){

            if(lyoko.getSuperscan() == null){getLogger().info("lyoko.getSuperscan() is null!");}
            if(this.getConfig() == null){getLogger().info("config is null");}
            if (this.getConfig().getConfigurationSection("interactors").getValues(false).isEmpty()){getLogger().info("configuration values are null");}
            lyoko.getSuperscan().setDetectors((HashMap<String, Object>) this.getConfig().getConfigurationSection("interactors").getValues(false));
            lyoko.getSuperscan().setActivators((HashMap<String,Object>) this.getConfig().getConfigurationSection("interactors").getValues(false));


    }
    public void storeConfig(){
        this.getConfig().createSection("interactors",lyoko.getSuperscan().serialize());
        saveConfig();
    }
    private void createConfig() {
        try {
            if (!getDataFolder().exists()) {
                getDataFolder().mkdirs();
            }
            File file = new File(getDataFolder(), "config.yml");
            if (!file.exists()) {
                getLogger().info("Config.yml not found, creating!");
                saveDefaultConfig();
            } else {
                getLogger().info("Config.yml found, loading!");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

}
